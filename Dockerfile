FROM python:3.9-buster
  
WORKDIR /app

# Install git & nginx
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git

# Clone martor repository
RUN git clone https://github.com/agusmakmun/django-markdown-editor.git

# Run martor
RUN cd django-markdown-editor && \
    python setup.py install && \
    cd martor_demo && \
    python manage.py makemigrations && \
    python manage.py migrate

EXPOSE 8000
CMD ["python","/app/django-markdown-editor/martor_demo/manage.py","runserver","0.0.0.0:8000"]